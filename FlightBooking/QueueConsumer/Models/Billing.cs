﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QueueConsumer.Models
{
    public partial class Billing
    {
        public Billing()
        {
            Bookings = new HashSet<Booking>();
        }

        public int BillingId { get; set; }
        public int DepartureSchedule { get; set; }
        public int ReturnSchedule { get; set; }
        public int UserId { get; set; }
        public double TotalAmount { get; set; }
        public int Discount { get; set; }
        public double FinalAmount { get; set; }
        public string Status { get; set; }
        public DateTime BillingDate { get; set; }
        public string JourneyType { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
