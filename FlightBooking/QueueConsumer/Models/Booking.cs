﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QueueConsumer.Models
{
    public partial class Booking
    {
        public int BookingId { get; set; }
        public int BillingId { get; set; }
        public int UserId { get; set; }
        public string FromPlace { get; set; }
        public string Toplace { get; set; }
        public DateTime JourneyDate { get; set; }
        public int TotalSeats { get; set; }
        public string Type { get; set; }
        public string BookingStatus { get; set; }
        public string Pnr { get; set; }
        public int Canceled { get; set; }
        public int ScheduleId { get; set; }
        public string SelectedSeats { get; set; }
        public double Amount { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual Billing Billing { get; set; }
    }
}
