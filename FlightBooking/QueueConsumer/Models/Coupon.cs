﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QueueConsumer.Models
{
    public partial class Coupon
    {
        public int CouponId { get; set; }
        public string CouponCode { get; set; }
        public int Percentage { get; set; }
        public int MaxCount { get; set; }
        public int UsedCount { get; set; }
        public DateTime Expiry { get; set; }
    }
}
