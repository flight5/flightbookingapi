﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QueueConsumer.Models
{
    public partial class Passenger
    {
        public int PassengerId { get; set; }
        public int BookingId { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string MealType { get; set; }
        public string SeatNumber { get; set; }
    }
}
