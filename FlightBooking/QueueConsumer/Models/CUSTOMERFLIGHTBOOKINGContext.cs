﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace QueueConsumer.Models
{
    public partial class CUSTOMERFLIGHTBOOKINGContext : DbContext
    {
        public CUSTOMERFLIGHTBOOKINGContext()
        {
        }

        public CUSTOMERFLIGHTBOOKINGContext(DbContextOptions<CUSTOMERFLIGHTBOOKINGContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Airline> Airlines { get; set; }
        public virtual DbSet<Billing> Billings { get; set; }
        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Coupon> Coupons { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Passenger> Passengers { get; set; }
        public virtual DbSet<Schedule> Schedules { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=(localdb)\\ProjectsV13;Database=CUSTOMERFLIGHTBOOKING;Integrated Security=True;MultipleActiveResultSets=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Airline>(entity =>
            {
                entity.ToTable("Airline");

                entity.Property(e => e.AirlineId).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.Logo).HasColumnName("logo");
            });

            modelBuilder.Entity<Billing>(entity =>
            {
                entity.ToTable("Billing");

                entity.Property(e => e.FinalAmount).HasColumnName("FInalAmount");

                entity.Property(e => e.JourneyType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Booking>(entity =>
            {
                entity.ToTable("Booking");

                entity.HasIndex(e => e.BillingId, "IX_Booking_BillingId");

                entity.Property(e => e.BookingStatus)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Billing)
                    .WithMany(p => p.Bookings)
                    .HasForeignKey(d => d.BillingId);
            });

            modelBuilder.Entity<Coupon>(entity =>
            {
                entity.HasIndex(e => e.CouponCode, "IX_Coupons_CouponCode")
                    .IsUnique()
                    .HasFilter("([CouponCode] IS NOT NULL)");

                entity.Property(e => e.CouponId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.userId);

                entity.Property(e => e.userId)
                    .ValueGeneratedNever()
                    .HasColumnName("userId");
            });

            modelBuilder.Entity<Passenger>(entity =>
            {
                entity.Property(e => e.MealType)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Schedule>(entity =>
            {
                entity.ToTable("Schedule");

                entity.HasIndex(e => e.AirlineId, "IX_Schedule_AirlineId");

                entity.Property(e => e.ScheduleId).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.MealsType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Airline)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.AirlineId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
