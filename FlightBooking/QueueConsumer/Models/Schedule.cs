﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QueueConsumer.Models
{
    public partial class Schedule
    {
        public int ScheduleId { get; set; }
        public int AirlineId { get; set; }
        public string FlightNumber { get; set; }
        public string FromPlace { get; set; }
        public string ToPlace { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ScheduledDays { get; set; }
        public int BussinesSeats { get; set; }
        public int TotalSeats { get; set; }
        public int TotalRows { get; set; }
        public string InstrumentsUsed { get; set; }
        public string MealsType { get; set; }
        public double TicketCost { get; set; }
        public int IsActive { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual Airline Airline { get; set; }
    }
}
