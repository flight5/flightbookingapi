﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QueueConsumer.Models
{
    public partial class Airline
    {
        public Airline()
        {
            Schedules = new HashSet<Schedule>();
        }

        public int AirlineId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Adress { get; set; }
        public string Logo { get; set; }
        public int Active { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}
