﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QueueConsumer.Models
{
    public partial class Customer
    {
        public int userId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
