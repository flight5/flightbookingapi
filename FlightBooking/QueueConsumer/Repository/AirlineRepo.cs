﻿using System;
using System.Linq;
using QueueConsumer.Models;

namespace QueueConsumer.Repository
{
    public class AirlineRepo :IAirlineRepo
    {
        private readonly CUSTOMERFLIGHTBOOKINGContext db;

        public AirlineRepo(CUSTOMERFLIGHTBOOKINGContext dbx)
        {
            db = dbx;
        }

       public  void UpdateAirline(Airline airline)
        {
            db.Airlines.Add(airline);
            db.SaveChanges();
        }

        public void ChangeAirlineStatus(int AirlineId, int Active)
        {
            var AirData = db.Airlines.First(x=>x.AirlineId== AirlineId);
            AirData.Active = Active;
            db.SaveChanges();


        }

        public void DeleteAirline(int AirlineId)
        {
            var AirData = db.Airlines.First(x => x.AirlineId == AirlineId);
            db.Airlines.Remove(AirData);
            db.SaveChanges();


        }

        public void AddNewSchedule(Schedule sche)
        {
            db.Schedules.Add(sche);
            db.SaveChanges();
        }

        public void DeleteSchedule(int scheId)
        {
            var SchedulesData = db.Schedules.First(x => x.ScheduleId == scheId);
            db.Schedules.Remove(SchedulesData);
            db.SaveChanges();


        }

        public void AddNewCoupon(Coupon cp)
        {
            db.Coupons.Add(cp);
            db.SaveChanges();
        }

        public void DeleteCoupon(int cpid)
        {
            var cpdata = db.Coupons.First(x => x.CouponId == cpid);
            db.Coupons.Remove(cpdata);
            db.SaveChanges();
        }

        public void AddnewUser(Customer user)
        {
            
                db.Customers.Add(user);
                db.SaveChanges();
            
            
        }

    }


    public interface IAirlineRepo
    {
        void UpdateAirline(Airline airline);
        void ChangeAirlineStatus(int AirlineId, int Active);
        void DeleteAirline(int AirlineId);
        void AddNewSchedule(Schedule sche);
        void DeleteSchedule(int scheId);
        void AddNewCoupon(Coupon cp);
        void DeleteCoupon(int cpid);
        void AddnewUser(Customer user);



    
    }


}


