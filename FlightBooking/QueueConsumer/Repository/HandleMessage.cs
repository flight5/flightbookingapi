﻿using System;
using System.Text;
using QueueConsumer.Models;
using Newtonsoft.Json;

namespace QueueConsumer.Repository
{
    public class HandleMessage
    {


       private AirlineRepo dbx;
        public HandleMessage()
        {
            dbx = new AirlineRepo(new CUSTOMERFLIGHTBOOKINGContext());

        }

        public void HandleBasicDeliver(String message)
        {
            Console.WriteLine(message);
            try
            {

                Console.WriteLine(message);

                QueeMessage JsonObject = JsonConvert.DeserializeObject<QueeMessage>(message);
                dynamic jsonData = JsonObject.Data;

                switch (JsonObject.Table)
                {

                    case "Airline":
                        if (JsonObject.Action == "Add")
                        {
                            //Console.WriteLine("Add");

                            var airline = new Airline();
                           airline.AirlineId = jsonData.AirlineId;
                            airline.Name = jsonData.Name;
                            airline.Phone = jsonData.Phone;
                            airline.Adress = jsonData.Adress;
                            airline.Active = jsonData.Active;
                            airline.Logo = jsonData.logo;
                            airline.CreatedDate = jsonData.createdDate;

                            dbx.UpdateAirline(airline);
                        }
                        else if (JsonObject.Action == "Active")
                        {
                            int airlineId = jsonData.AirlineId;
                            int active = jsonData.Active;
                            dbx.ChangeAirlineStatus(airlineId, active);




                        }
                        else if (JsonObject.Action == "Delete")
                        {
                            int airlineId = jsonData.AirlineId;
                            dbx.DeleteAirline(airlineId);

                        }
                        break;

                    case "Schedule":
                        if (JsonObject.Action == "Add")
                        {

                            var scheduledt = new Schedule();
                            scheduledt.AirlineId = jsonData.AirlineId;
                            scheduledt.FlightNumber = jsonData.FlightNumber;
                            scheduledt.FromPlace = jsonData.FromPlace;
                            scheduledt.ToPlace = jsonData.ToPlace;
                            scheduledt.StartDate = jsonData.StartDate;
                            scheduledt.EndDate = jsonData.EndDate;
                            scheduledt.ScheduledDays = jsonData.ScheduledDays;
                            scheduledt.BussinesSeats = jsonData.BussinesSeats;
                            scheduledt.TotalSeats = jsonData.TotalSeats;
                            scheduledt.TotalRows = jsonData.TotalRows;
                            scheduledt.InstrumentsUsed = jsonData.InstrumentsUsed;
                            scheduledt.MealsType = jsonData.MealsType;
                            scheduledt.TicketCost = jsonData.TicketCost;
                            scheduledt.IsActive = jsonData.IsActive;
                            scheduledt.ScheduleId = jsonData.ScheduleId;
                            dbx.AddNewSchedule(scheduledt);

                        }
                        else if (JsonObject.Action == "Delete")
                        {
                            int scheduleId = jsonData.ScheduleId;
                            dbx.DeleteSchedule(scheduleId);

                        }

                        break;

                    case "Coupons":
                        if (JsonObject.Action == "Add")
                        {

                            var couponDt = new Coupon();
                            couponDt.CouponCode = jsonData.CouponCode;
                            couponDt.Percentage = jsonData.Percentage;
                            couponDt.MaxCount = jsonData.MaxCount;
                            couponDt.UsedCount = 0;
                            couponDt.Expiry = jsonData.Expiry;
                            couponDt.CouponId = jsonData.CouponId;
                            dbx.AddNewCoupon(couponDt);

                        }
                        else if (JsonObject.Action == "Delete")
                        {
                            int couponId = jsonData.CouponId;
                            dbx.DeleteCoupon(couponId);

                        }

                        break;

                    case "Customer":
                        if (JsonObject.Action == "Add")
                        {

                            var custmer = new Customer();
                            custmer.userId = jsonData.UserId;
                            custmer.Name = jsonData.Name;
                            custmer.Email = jsonData.Email;
                            custmer.Phone = jsonData.PhoneNumber;

                            dbx.AddnewUser(custmer);

                        }

                        break;


                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }






        }


    }

    // Scaffold-DbContext "Server=(localdb)\\ProjectsV13;Database=CUSTOMERFLIGHTBOOKING;Integrated Security=True;MultipleActiveResultSets=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models
}
