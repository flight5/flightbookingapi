﻿using QueueConsumer.Repository;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueConsumer
{
    public static class Consumer
    {
        private static string quee = "FLBDBSYNC";
        public static void consume(IModel channel)
        {

            HandleMessage messageHandler = new HandleMessage();

            channel.QueueDeclare(Consumer.quee,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                Console.WriteLine(message);
              messageHandler.HandleBasicDeliver(message);
                channel.BasicAck(e.DeliveryTag, false);


            };


            // 

            channel.BasicConsume(Consumer.quee, false, consumer);

            Console.WriteLine("Consumer Started");
            Console.ReadLine();

        }
    }
}
