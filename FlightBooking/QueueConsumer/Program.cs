﻿using RabbitMQ.Client;
using System;

namespace QueueConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var factory = new ConnectionFactory
                {
                    Uri = new System.Uri("amqp://guest:guest@localhost:5672")
                };

                var connection = factory.CreateConnection();
                var channel = connection.CreateModel();
                Consumer.consume(channel);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);

            }
        }
    }
}
