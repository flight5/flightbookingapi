﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using CustomerServices.Models;
using CustomerServices.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CustomerServices.Controllers
{
    [Route("api/")]
    [ApiController]
    public class PassengerBooking : Controller
    {

        private readonly IConfiguration _configuration;
        private IBooking bdb;

        public PassengerBooking(IConfiguration configuration, CoustmerDbContext dbx)
        {
            _configuration = configuration;
            bdb = new Booking(dbx);

        }

        [HttpPost("booking/started"), Authorize(Roles = "User")]
        public async Task<ActionResult> getScheduleDetail(BillingData bdata)
        {

            int userId = getUser();

            var tripType = JourneyTypes.OneWay;
            if (bdata.Departure == 0 && bdata.Return == 0)
            {
                return BadRequest("Not a valid schedule ");
            }
            if (bdata.Departure > 0 && bdata.Return > 0)
            {
                tripType = JourneyTypes.RoundTrip;

            }

            var BillingObj = new Billing();
            BillingObj.DepartureSchedule = bdata.Departure;
            BillingObj.ReturnSchedule = bdata.Return;
            BillingObj.UserId = userId;
            BillingObj.JourneyType = tripType;

            var billingId = bdb.createNewbillingId(BillingObj);

            if (billingId == 0)
            {
                return BadRequest("Unbale to create the billing");

            }
            var msg = new { BillingId = billingId, BillingData = bdata };

            CustomerModel userData = new CustomerModel();
            //var identity = HttpContext.User.Identity as ClaimsIdentity;
            //if (identity != null)
            //{
              //  IEnumerable<Claim> claims = identity.Claims;
                userData.Email = User.FindFirstValue(ClaimTypes.Email);
                userData.Phone = User.FindFirstValue(ClaimTypes.MobilePhone);
                userData.Name = User.FindFirstValue(ClaimTypes.Name);
                userData.userId = userId;
                bdb.updateCoustmers(userData);


            //}

            return Ok(msg);

        }

       /* [HttpPost("new/bookings"), Authorize(Roles = "User")]
        public async Task<ActionResult> newBookings(BookingData bdata)
        {

            int userId = getUser();

            if (bdata.BillingId == 0)
            {
                return BadRequest("please provide valid billing data");
            }



            BookingModel BookObj = new BookingModel();

            TravelType key = (TravelType)Enum.Parse(typeof(TravelType), bdata.Type);

            BookObj.ScheduleId = bdata.ScheduleId;
            BookObj.BillingId = bdata.BillingId;
            BookObj.TotalSeats = bdata.TotalSeats;
            BookObj.Type = key;
            BookObj.FromPlace = "Thodupuza";
            BookObj.Toplace = "Idukki";
            BookObj.UserId = userId;

            BookObj.JourneyDate = DateTime.Now;
            var totalAmnt = 1000 * bdata.TotalSeats;
            BookObj.Amount = totalAmnt;

            var bookingId = bdb.StartBooking(BookObj);
            if (bookingId == 0)
            {
                return BadRequest("Unbale to start booking");


            }

           return  Ok(bookingId);


        }*/


        [HttpPost("booking"), Authorize(Roles = "User")]
        public async Task<ActionResult> AddBookData(BookingData bdata)
        {

            int userId =getUser();

            if (bdata.BillingId == 0) {
                return BadRequest("please provide valid billing data");
            }

            CustomerModel userData = new CustomerModel();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;
                userData.Email= identity.FindFirst(ClaimTypes.Email).Value;
                userData.Phone = identity.FindFirst(ClaimTypes.MobilePhone).Value;
                userData.Name = identity.FindFirst(ClaimTypes.Name).Value;
                userData.userId = userId;
               // bdb.updateCoustmers(userData);


            }


            Schedule SchData = bdb.getScheduleData(bdata.ScheduleId);

            BookingModel BookObj = new BookingModel();

            TravelType key = (TravelType)Enum.Parse(typeof(TravelType), bdata.Type);

            BookObj.ScheduleId = bdata.ScheduleId;
            BookObj.BillingId = bdata.BillingId;
            BookObj.TotalSeats = bdata.TotalSeats;
            BookObj.Type = key;
            BookObj.FromPlace = SchData.FromPlace;
            BookObj.Toplace = SchData.ToPlace;
            BookObj.UserId = userId;
            BookObj.JourneyDate = SchData.StartDate;
            var totalAmnt = SchData.TicketCost * bdata.TotalSeats;
            BookObj.Amount = totalAmnt;

            var bookingId =   bdb.StartBooking(BookObj);
            if (bookingId == 0)
            {
                return BadRequest("Unbale to start booking");

            }

            if (bdata.Passenger.Count <= 0)
            {
                return BadRequest("Please provide passenger details");

            }

            string BookedSeats = "";

                foreach (PassengerData pass in bdata.Passenger)
                {
                    Passenger pdata = new Passenger();

                    pdata.BookingId = bookingId;
                    pdata.Gender = pass.Gender;
                    pdata.SeatNumber = pass.SeatNumber;
                    pdata.Name = pass.Name;
                    pdata.Age = pass.Age;
                    pdata.MealType = pass.MealType;
                    bdb.addPassenger(pdata);
                    BookedSeats = BookedSeats + pass.SeatNumber + ",";

                }
                BookedSeats = BookedSeats.TrimEnd(',');
                BookingModel BookObj2 = new BookingModel();
                BookObj2.BookingId = bookingId;
                BookObj2.SelectedSeats = BookedSeats;
                bdb.updateBookingStatus(BookObj2);
            

            var BillingObj = new Billing();
            BillingObj.TotalAmount = totalAmnt;
            BillingObj.FInalAmount = totalAmnt;
            BillingObj.BillingId = bdata.BillingId;
            BillingObj.Status = BillingStatus.Booking;
            bdb.updateBillingAmounts(BillingObj);

            var msg = new { BookingId = bookingId, msg = "Sucessfully booked data" };

            return Ok(msg);

        }
        [HttpGet("booking/pricedetails"), Authorize(Roles = "User")]
        public async Task<ActionResult> getPayment(int BillingId)
        {
            if (BillingId == 0)
            {
                return BadRequest("Please provide a valid request");

            }
            var billingdata = bdb.getbillamount(BillingId);
            return Ok(billingdata);


        }
        [HttpPost("coupon/apply"), Authorize(Roles = "User")]
        public async Task<ActionResult> getCouponDiscounts(String Coupon, int BillId)
        {
            if (String.IsNullOrEmpty(Coupon))
            {
                return BadRequest("Please provide a valid request");

            }

            int userId = getUser();

            var CouponData = bdb.getDiscountCoupon(Coupon);
            if (CouponData == null)
            {
                return BadRequest("Please provide a valid coupon");

            }

            if (CouponData.UsedCount >= CouponData.MaxCount)
            {
                return BadRequest("Coupon code is fully used");

            }

            if (CouponData.Expiry < DateTime.Now)
            {
                return BadRequest("Coupon code is expired");

            }

            int discount = CouponData.Percentage;

            if (bdb.UpdateBilling(BillId, userId, discount))
            {

                bdb.UpdateCoupon(CouponData.CouponId);
                return Ok("Sucessfully applied the coupon.");

            }
            else
            {
                return BadRequest("Unbale to apply the coupon");


            }





        }


        [HttpPost("booking/pay"), Authorize(Roles = "User")]
        public async Task<ActionResult> CompletePayment(int BillingId)
        {
            if (BillingId == 0)
            {
                return BadRequest("Please provide a valid request");

            }

            int userId = getUser();

            var BookingData = bdb.getBookings(BillingId, userId);
            if (BookingData == null)
            {
                return BadRequest("Please provide a valid  BillingId");


            }
            foreach (BookingModel bmodel in BookingData)
            {
                int bookId = bmodel.BookingId;
                string pnr = generatePNR(bookId);
                if (bmodel.BookingStatus == BookingState.Initiated)
                {
                    bdb.CompleteBooking(bookId, pnr, userId);
                }


            }

            bdb.CompleteBilling(BillingId, userId);
            var msg = new { msg = "Sucessfully Gnerated the PNR for the user." };
            return Ok(msg);


        }



        [HttpGet("search/bookings"), Authorize(Roles = "User")]
        public async Task<ActionResult> SearchPnr(String SearchItems,String SearchType="pnr")
        {
            if (String.IsNullOrEmpty(SearchItems))
            {
                return BadRequest("Please provide a valid request");

            }

            int userId = getUser();

            var BookingData = bdb.SearchBookings(SearchItems, SearchType, userId);
            if (BookingData == null)
            {
                return BadRequest("Please provide a valid  BillingId");


            }
           
            return Ok(BookingData);


        }
        /*[HttpGet("emailsearch/booking"),Authorize(Roles ="User")]
        public async Task<ActionResult> getemailBooking(string emailId)
        {
            if (String.IsNullOrEmpty(emailId))
            {
                return BadRequest("Please provide a valid request");

            }
            List<dynamic> BookingData =bdb.emailsearch(emailId);
            if (BookingData == null)
            {
                return BadRequest("Please provide a valid  Emailid");


            }

            return Ok(BookingData);
        }*/

        [HttpGet("myall/bookings"), Authorize(Roles = "User")]
        public async Task<ActionResult> getAllBookings()
        {
            
            int userId = getUser();

            var BookingData = bdb.SearchAllBookings(userId);
            if (BookingData == null)
            {
                return BadRequest("Booking not found");


            }

            return Ok(BookingData);


        }



        [HttpGet("pnr/cancel"), Authorize(Roles = "User")]
        public async Task<ActionResult> CancelPnr(String pnr)
        {
            if (String.IsNullOrEmpty(pnr))
            {
                return BadRequest("Please provide a valid request");

            }

            int userId = getUser();

            var BookingData = bdb.getuserBooking(pnr, userId);
            if (BookingData == null)
            {
                return BadRequest("Please provide a valid  PNR");


            }

            bdb.cancelBookings(BookingData.BookingId, userId);
            if (bdb.CancelBookingCount(BookingData.BookingId, userId) <= 0) {
                bdb.cancelBilling(BookingData.BillingId, userId);


            }
            var msg = new { msg = "Sucessfully canceled the bookings." };
            return Ok(msg);


        }



        
        private int  getUser()
        {
            String userId;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;
                userId = identity.FindFirst(ClaimTypes.UserData).Value;
                return Int32.Parse(userId);

            }
            throw new UnauthorizedAccessException("Please provide valid user");

        }


        private string generatePNR(int bookId)
        {

            Random rand = new Random();
            int stringlen = rand.Next(4, 10);
            int randValue;
            string str = "";
            char letter;
            for (int i = 0; i < stringlen; i++)
            {
                randValue = rand.Next(0, 26);
                letter = Convert.ToChar(randValue + 65);

                str = str + letter;
            }
            str = bookId + "00" + str;
            return str;
        }


    }
}
