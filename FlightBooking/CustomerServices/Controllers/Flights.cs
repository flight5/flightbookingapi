﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CustomerServices.Repository;
using Microsoft.Extensions.Configuration;
using CustomerServices.Models;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CustomerServices.Controllers
{
    [Route("api/")]
    [ApiController]
    public class Flights : Controller
    {

        private readonly IConfiguration _configuration;
        private IFlights fdb;

        public Flights(IConfiguration configuration, CoustmerDbContext userDbCtx)
        {
            _configuration = configuration;
            fdb = new FlightsRepo(userDbCtx);

        }
        [HttpGet("search")]
        public async Task<ActionResult> serachFlights(String FromPlace, String ToPlace, int roundTrip = 0, String returnDate=null,String DepatureDate= null)
        {


            if (String.IsNullOrEmpty(FromPlace))
            {
                return BadRequest("Please provide a valid journey place.");
            }

            if (String.IsNullOrEmpty(ToPlace))
            {
                return BadRequest("Please provide a valid return journey place.");
            }



            if (roundTrip == 1)
            {
                if (String.IsNullOrEmpty(DepatureDate))
                {
                    return BadRequest("Please provide a valid journey date.");
                }

                if (String.IsNullOrEmpty(returnDate))
                {
                    return BadRequest("Please provide a valid return  date.");
                }

            }
            var returnDays = new List<SearchData>();

            var journeyDys = fdb.serachFlights(FromPlace, ToPlace, DepatureDate);
            if (roundTrip == 1)
            {
                 returnDays = fdb.serachFlights(ToPlace, FromPlace, returnDate);

            }
       
          
            var serachItems = new { Depature = journeyDys, returns= returnDays };



            return Ok(serachItems);

        }


        [HttpGet("schedule")]
        public async Task<ActionResult> getSchedule(int scheduleId)
        {
            if (scheduleId == 0)
            {
                return BadRequest("Not a valid schedule ");
            }

            var scheduleData = fdb.GetScheduleById(scheduleId);
            if (scheduleData == null)
            {
                return BadRequest("Please provide a valid schedule.");
            }
            return Ok(scheduleData);

        }


        [HttpGet("schedule/selected")]
        public async Task<ActionResult> getScheduleDetail(int departureId,int returnId)
        {
            if (departureId == 0)
            {
                return BadRequest("Not a valid schedule ");
            }
            Double totalprice = 0;
            var scheduleData = fdb.GetScheduleById(departureId);

            if (scheduleData != null)
            {
                totalprice = totalprice + scheduleData.TicketCost;
            }

            var returnData = fdb.GetScheduleById(returnId);

            if (returnData != null)
            {
                totalprice = totalprice + returnData.TicketCost;
            }

            var result = new { Depature = scheduleData, returns = returnData,totolCost= totalprice };
            return Ok(result);

        }
    }
}
