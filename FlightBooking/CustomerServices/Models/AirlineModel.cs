﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace CustomerServices.Models
{
    public class AirlineModel
    {
      
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AirlineId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Adress { get; set; }
        public string logo { get; set; }
        public int Active { get; set; } = 1;
        public DateTime createdDate { get; set; } = DateTime.Now;


      
    }


    public enum MealsTypes
    {
        Veg,
        NonVeg,
        Both
    }


    public class Schedule
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ScheduleId { get; set; }

        public AirlineModel Airline { get; set; }
        [ForeignKey("AirlineId")]
        public int AirlineId { get; set; }
        public string FlightNumber { get; set; }
        public string FromPlace { get; set; }
        public string ToPlace { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ScheduledDays { get; set; } = "Daily"; //Weakly,Daily,Monday,Tuesday,Wensday..etc
        public int BussinesSeats { get; set; }
        public int TotalSeats { get; set; }
        public int TotalRows { get; set; }
        public string InstrumentsUsed { get; set; }
        public MealsTypes MealsType { get; set; }
        public Double TicketCost { get; set; }

        public int IsActive { get; set; } = 1;
        public DateTime createdDate { get; set; } = DateTime.Now;

    }



    public class SearchData
    {

        public int AirlineId { get; set; }
        public int ScheduleId { get; set; }
        public string FlightNumber { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string logo { get; set; }
        public string FromPlace { get; set; }
        public string ToPlace { get; set; }
        public DateTime StartDate { get; set; }
        public Double TicketCost { get; set; }
        public int JourneyDays { get; set; }



    }


    public class Coupons
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CouponId { get; set; }
        public string CouponCode { get; set; }
        public int Percentage { get; set; }
        public int MaxCount { get; set; }
        public int UsedCount { get; set; }
        public DateTime Expiry { get; set; }
    }

    

}
