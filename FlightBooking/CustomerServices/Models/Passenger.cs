﻿using System;
namespace CustomerServices.Models
{

    public enum MealTypes
    {
        Veg,
        Nonveg
    }

    public class Passenger
    {
        public int PassengerId { get; set; }
        public int BookingId { get; set; }
        public String Name { get; set; }
        public String Gender { get; set; }
        public int Age { get; set; }
        public MealTypes MealType { get; set; } 
        public String SeatNumber { get; set; }

    }

    public class PassengerData
    {
        public String Name { get; set; }
        public String Gender { get; set; }
        public int Age { get; set; }
        public MealTypes MealType { get; set; }
        public String SeatNumber { get; set; }

    }


}
