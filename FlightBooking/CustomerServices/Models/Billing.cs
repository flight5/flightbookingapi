﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CustomerServices.Models
{

    public enum JourneyTypes
    {
        OneWay,
        RoundTrip
    }

    public enum BillingStatus
    {
        Initiate,
        Booking,
        Paid,
        Canceled

    }


    public class Billing
    {
      
            [Key]
            public int BillingId { get; set; }
            public int DepartureSchedule { get; set; }
            public int ReturnSchedule { get; set; }
            public int UserId { get; set; }
            public Double TotalAmount { get; set; }
            public int Discount { get; set; }
            public Double FInalAmount { get; set; }
            public BillingStatus Status { get; set; } = BillingStatus.Initiate;
            public DateTime BillingDate {get;set; }
            public JourneyTypes JourneyType { get; set; } = JourneyTypes.OneWay;
            public DateTime CreatedDate { get; set; } = DateTime.Now;

        

       

     }


    public class BillingData
    {
        [Required]
        public int Departure { get; set; }
        [Required]
        public int Return { get; set; }


    }




}
