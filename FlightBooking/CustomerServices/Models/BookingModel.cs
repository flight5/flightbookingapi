﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerServices.Models
{

    public enum TravelType {
        Departure,
        Return

    }

    public enum BookingState  {
        Initiated,
        Pnr,
        Canceled

    }
    public class BookingModel

    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookingId { get; set; }
        public Billing Billing { get; set; }
        [ForeignKey("Billing")]
        public int BillingId { get; set; }
        public int UserId { get; set; }
        public String FromPlace { get; set; }
        public String Toplace { get; set; }
        public DateTime JourneyDate { get; set; }

        public int TotalSeats { get; set; }
        public TravelType Type { get; set; } = TravelType.Departure; //Departure/Return
        public BookingState BookingStatus { get; set; } = BookingState.Initiated;

        public String Pnr { get; set; }
        public int Canceled { get; set; } = 0;
        public int ScheduleId { get; set; }
        public String SelectedSeats { get; set; }
        public Double Amount { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }


    public class BookingData
    {

        public int BillingId { get; set; }
        public int TotalSeats { get; set; }
        public String Type { get; set; } //Departure/Return
        public int ScheduleId { get; set; }
        public List<PassengerData> Passenger { get; set; }
       
    }
}
