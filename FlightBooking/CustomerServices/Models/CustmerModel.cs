﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerServices.Models
{
    public class CustomerModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int userId { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
    
    }
}
