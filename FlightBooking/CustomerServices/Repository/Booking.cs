﻿using System;
using System.Collections.Generic;
using System.Linq;
using CustomerServices.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomerServices.Repository
{
    public class Booking : IBooking
    {
        private readonly CoustmerDbContext db;
        public Booking(CoustmerDbContext dbCtx)
        {
            db = dbCtx;
        }

        public int createNewbillingId(Billing BillingObj)
        {

            try
            {

                var dbs = db.Billing.Update(BillingObj);
                db.SaveChanges();
                return dbs.Entity.BillingId;


            }
            catch (Exception a)
            {
                return 0;
            }


        }

        public Schedule getScheduleData(int bookId)
        {

            try
            {

                return db.Schedule.Where(x => x.ScheduleId == bookId).FirstOrDefault();


            }
            catch (Exception a)
            {
                return null;
            }

        }


        public  int StartBooking(BookingModel bdata)
        {
            try
            {

                var dbs = db.Booking.Update(bdata);
                db.SaveChanges();
                return dbs.Entity.BookingId;
            }
            catch (Exception a)
            {
                return 0;
            }
        }


        public void addPassenger(Passenger pdata)
        {

            try
            {

                var dbs = db.Passengers.Update(pdata);
                db.SaveChanges();


            }
            catch (Exception a)
            {
            }


        }


        public List<BookingModel> getBookings(int BillingId, int userId)
        {
            try
            {

                return db.Booking.Where(x => x.BillingId == BillingId && x.UserId == userId).ToList<BookingModel>();


            }
            catch (Exception a)
            {
                return null;
            }

        }



        public void CompleteBooking(int BookingId, String Pnr, int userId)
        {
            BookingModel Bdetails = db.Booking.First(x => x.BookingId == BookingId && x.UserId == userId);
            Bdetails.Pnr = Pnr;
            Bdetails.BookingStatus = BookingState.Pnr;
            db.SaveChanges();

        }

        public void CompleteBilling(int BillingId, int userId)
        {

            Billing Bdetails = db.Billing.First(x => x.BillingId == BillingId && x.UserId == userId);
            Bdetails.Status = BillingStatus.Paid;
            Bdetails.BillingDate = DateTime.UtcNow;
            db.SaveChanges();

        }


        public List<BookingModel> SearchBookings(String SearchItems, String SearchType, int userId)
        {

            if (SearchType == "pnr")
            {
                return db.Booking.Where(x => x.Pnr == SearchItems && x.UserId == userId).ToList<BookingModel>();


            }
            else
            {

                return    (from p in db.Booking
                          join c in db.Customers 
                          on p.UserId equals c.userId 
                          where c.Email.StartsWith(SearchItems)
                          select p).ToList<BookingModel>();

            }

        }


        public List<BookingModel> SearchAllBookings(int userId)
        {
            return db.Booking.Where(x => x.UserId == userId).ToList<BookingModel>();


        }

        public BookingModel getuserBooking(string pnr, int userId)
        {
            BookingModel Bdetails = db.Booking.First(x => x.Pnr == pnr && x.UserId == userId );
            return Bdetails;


        }

        public void cancelBookings(int BookingId,int userId)
        {

            BookingModel Bdetails = db.Booking.First(x => x.BookingId == BookingId && x.UserId == userId);
            Bdetails.BookingStatus = BookingState.Canceled;
            Bdetails.Canceled = 1;
            db.SaveChanges();
        }


        public int CancelBookingCount(int BookingId, int userId)
        {

             return db.Booking.Where(x => x.BookingId == BookingId && x.UserId == userId && x.Canceled==1).Select(r=>r).Count();

        }

        public void cancelBilling(int BillingId, int userId)
        {

            Billing Bdetails = db.Billing.First(x => x.BillingId == BillingId && x.UserId == userId);
            Bdetails.Status = BillingStatus.Canceled;
            db.SaveChanges();
        }


        public Coupons getDiscountCoupon(string coupon)
        {
            return db.Coupons.First(x => x.CouponCode == coupon);

        }

        public Billing getuserBilling(int BillId, int userId)
        {
            return db.Billing.First(x => x.BillingId == BillId && x.UserId == userId);

        }

        public Boolean UpdateBilling(int BillId, int userId,int discount)
        {

            try
            {
                Billing Bdetails = db.Billing.First(x => x.BillingId == BillId && x.UserId == userId);

                var finalAmt = Math.Ceiling(Bdetails.FInalAmount * discount / 100);
                Bdetails.Discount = discount;
                Bdetails.FInalAmount = Bdetails.FInalAmount - finalAmt;
                db.SaveChanges();
                return true;

            }
            catch (Exception a)
            {
                return false;
            }



        }

        public void UpdateCoupon(int cid)
        {
            Coupons cp = db.Coupons.First(x => x.CouponId == cid);
            cp.UsedCount = cp.UsedCount + 1;
            db.SaveChanges();


        }

        public void updateBillingAmounts(Billing billobj)
        {

            Billing cp = db.Billing.First(x => x.BillingId == billobj.BillingId);
            cp.TotalAmount = cp.TotalAmount + billobj.TotalAmount;
            cp.FInalAmount = cp.FInalAmount + billobj.FInalAmount;
            cp.Status = billobj.Status;
            db.SaveChanges();
        }



        public void updateBookingStatus(BookingModel bookObj)
        {

            BookingModel cp = db.Booking.First(x => x.BookingId == bookObj.BookingId);
            cp.SelectedSeats = bookObj.SelectedSeats;
            db.SaveChanges();
        }

        public void updateCoustmers(CustomerModel udata)
        {

            try
            {
                int userExists = db.Customers.Where(x => x.userId == udata.userId).Select(r => r).Count();
                if (userExists <= 0)
                {
                    var dbs = db.Customers.Update(udata);
                    db.SaveChanges();
                }


            }
            catch (Exception a)
            {
            }


        }
        public Billing getbillamount(int billingId)
        {
            return db.Billing.First(x => x.BillingId == billingId);
        }


        public List<dynamic> emailsearch(string emailId)
        {

            var bookdetails = (from e in db.Customers
                               join p in db.Booking on
                               e.userId equals p.UserId
                               where e.Email == emailId
                               select new
                               {
                                   Name = e.Name,
                                   BookingId = p.BookingId,
                                   BillingId = p.BillingId,
                                   FromPlace = p.FromPlace,
                                   ToPlace = p.Toplace,
                                   JourneyDate = p.JourneyDate,
                                   BookingStatus = p.BookingStatus,
                                   PNR = p.Pnr,
                               }
                             ).ToList<dynamic>();
            return bookdetails;
        }



    }
}
