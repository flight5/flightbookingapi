﻿using System;
using System.Collections.Generic;
using CustomerServices.Models;

namespace CustomerServices.Repository
{
    public interface IFlights
    {
        List<SearchData> serachFlights(String FromPlace, String ToPlace, String JourneyDate);
        Schedule GetScheduleById(int sid);

    }
}
