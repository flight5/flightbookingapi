﻿using System;
using Microsoft.EntityFrameworkCore;
using CustomerServices.Models;

namespace CustomerServices.Repository
{
    public class CoustmerDbContext: DbContext
    {
        public CoustmerDbContext(DbContextOptions<CoustmerDbContext> options) : base(options)
        {
        }



        public DbSet<BookingModel> Booking { get; set; }
        public DbSet<AirlineModel> Airline { get; set; }
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<Billing> Billing { get; set; }
        public DbSet<CustomerModel> Customers { get; set; }
        public DbSet<Passenger> Passengers { get; set; }
        public DbSet<Coupons> Coupons { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Schedule>()
            .Property(u => u.MealsType)
            .HasConversion<string>()
            .HasMaxLength(50);

            modelBuilder.Entity<Billing>()
             .Property(u => u.JourneyType)
             .HasConversion<string>()
             .HasMaxLength(50);


            modelBuilder.Entity<Billing>()
             .Property(u => u.Status)
             .HasConversion<string>()
             .HasMaxLength(50);

            modelBuilder.Entity<BookingModel>()
                .Property(u => u.Type)
                .HasConversion<string>()
                .HasMaxLength(50);

            modelBuilder.Entity<BookingModel>()
               .Property(u => u.BookingStatus)
               .HasConversion<string>()
               .HasMaxLength(50);

            modelBuilder.Entity<Passenger>()
               .Property(u => u.MealType)
               .HasConversion<string>()
               .HasMaxLength(50);
            

            modelBuilder.Entity<Coupons>()
             .HasIndex(p => new { p.CouponCode }).IsUnique();


        }


    }
}
