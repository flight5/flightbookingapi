﻿using System;
using System.Collections.Generic;
using CustomerServices.Models;

namespace CustomerServices.Repository
{

    public interface IBooking
    {
        int createNewbillingId(Billing BillingObj);
        Schedule getScheduleData(int bookId);
        int StartBooking(BookingModel bdata);
        void addPassenger(Passenger pdata);
        List<BookingModel> getBookings(int BillingId, int userId);
        void CompleteBooking(int BookingId, String Pnr, int userId);
        void CompleteBilling(int BillingId, int userId);
        List<BookingModel> SearchBookings(String SearchItems, String SearchType, int userId);

        List<BookingModel>  SearchAllBookings(int userId);
        BookingModel  getuserBooking(string pnr,int userId);
        void cancelBookings(int BookingId, int userId);
        int CancelBookingCount(int BookingId, int userId);
        void cancelBilling(int BillingId,int userId);
        Coupons getDiscountCoupon(string coupon);
        Boolean UpdateBilling(int BillId, int userId,int discount);
        void UpdateCoupon(int cid);
        void updateBillingAmounts(Billing billAmt);
        void updateBookingStatus(BookingModel bookObj);
        void updateCoustmers(CustomerModel udata);
        Billing getbillamount(int billingId);
        List<dynamic> emailsearch(string emailId);

    }
}
