﻿using System;
using System.Collections.Generic;
using System.Linq;
using CustomerServices.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomerServices.Repository
{
    public class FlightsRepo: IFlights
    {
        private readonly CoustmerDbContext db;
        public FlightsRepo(CoustmerDbContext dbCtx)
        {
            db = dbCtx;
        }

        public List<SearchData> serachFlights(String FromPlace, String ToPlace, String JourneyDate=null)
        {



             var mainQuery = db.Schedule.Include(r => r.Airline).Select(r=>r) ;

            if (String.IsNullOrEmpty(JourneyDate))
            {


                mainQuery = mainQuery.Where(r => r.FromPlace.Equals(FromPlace) && r.ToPlace.Equals(ToPlace) && (r.Airline.Active == 1));
            }
            else
            {
                DateTime oDate = Convert.ToDateTime(JourneyDate);

                mainQuery=  mainQuery.Where(r => r.FromPlace.Equals(FromPlace) && r.ToPlace.Equals(ToPlace) && (r.Airline.Active == 1)  && r.StartDate.Date.Equals(oDate.Date));


            }
          List<SearchData>  data =  mainQuery.Select(s =>new SearchData()
          {
              AirlineId = s.AirlineId,
              ScheduleId = s.ScheduleId,
                       Name=s.Airline.Name,
                       FlightNumber=s.FlightNumber,
                       FromPlace=s.FromPlace,
                       ToPlace=s.ToPlace,
                       StartDate=s.StartDate,
                       logo=s.Airline.logo,
                       TicketCost=s.TicketCost,
                       Phone=s.Airline.Phone,
                       JourneyDays=EF.Functions.DateDiffDay(s.StartDate,s.EndDate)

          }).OrderByDescending(x => x.StartDate).ToList<SearchData>();
            
            return data;

            // db.Database.SqlQuery()
        }

        public  Schedule GetScheduleById(int sid)
        {
            try
            {
                return db.Schedule.Where(x => x.ScheduleId == sid).FirstOrDefault();

            }
            catch (Exception e)
            {
                return null;
            }


        }
    }


  
}
