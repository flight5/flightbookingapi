﻿using AdminServices.Models;
using AdminServices.Repository;
using AdminServices.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Controllers
{
    [Route("admin/")]
    [ApiController]
    public class CouponController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ICouponRepository coupondb;
        private readonly IDatabaseSync quee;

        public CouponController(IConfiguration configuration, AdminDbContext adminDbCtx, IDatabaseSync dbquee)
        {
            _configuration = configuration;
            coupondb = new CouponRepository(adminDbCtx);
            quee = dbquee;

        }
        [HttpPost("coupon/add"), Authorize(Roles = "Admin")]
        public async Task<ActionResult> AddCoupon(CouponData request)
        {
            var couponDt = new Coupons();
            couponDt.CouponCode = request.CouponCode;
            couponDt.Percentage = request.Percentage;
            couponDt.MaxCount = request.MaxCount;
            couponDt.UsedCount = 0;
            couponDt.Expiry = request.Expiry;
            int cpobj = coupondb.AddCoupon(couponDt);
            if (cpobj !=0)
            {
                couponDt.CouponId = cpobj;
                var msg = new { Table = "Coupons", Action = "Add", Data = couponDt };
                quee.publish(msg);
                return Ok("Coupon created successfully");

            }
            return BadRequest("Unable to create coupon.");
        }
        [HttpGet("coupon/getcoupon"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> CouponDetails(int couponId)
        {
            var couponList = coupondb.getCoupon(couponId);
            if (couponList != null)
            {
                return Ok(couponList);
            }
            else
            {
                return Ok("Coupon not found");
            }
        }
        [HttpGet("coupon/getallcoupon"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> AllCouponDetails()
        {
            var couponList = coupondb.getallCoupon();
            if (couponList != null)
            {
                return Ok(couponList);
            }
            else
            {
                return Ok("Coupon not found");
            }
        }
        [HttpDelete("coupon/remove"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteCoupon(int couponId)
        {
            var result = coupondb.dltcoupon(couponId);
            if (result)
            {

                var newObj = new { CouponId = couponId };

                var msg = new { Table = "Coupons", Action = "Delete", Data = newObj };
                quee.publish(msg);
                return Ok("Coupon deleted successfully");
            }
            else
            {
                return Ok("Failed to delete coupon");
            }

        }
    }
}
