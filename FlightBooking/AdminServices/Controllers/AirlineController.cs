﻿using AdminServices.Models;
using AdminServices.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminServices.Services;

namespace AdminServices.Controllers
{
    [Route("admin/")]
    [ApiController]
    public class AirlineController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private IAirlineRepository airlinedb;
        private readonly IDatabaseSync quee;

        public AirlineController(IConfiguration configuration, AdminDbContext adminDbCtx,IDatabaseSync dbquee)
        {
            _configuration = configuration;
            airlinedb = new AirlineRepository(adminDbCtx);
            quee = dbquee;

        }
        [HttpPost("airline/add"), Authorize(Roles = "Admin")]
        public async Task<ActionResult> AddAirline(AirlineData request)
        {
            var airlinedt = new Airline();
            airlinedt.Name = request.Name;
            airlinedt.Phone = request.Phone;
            airlinedt.Adress = request.Adress;
            airlinedt.logo = request.logo;
            airlinedt.Active = request.Active;

            int airlineId = airlinedb.AddAirline(airlinedt);
            if (airlineId != 0)
            {
                airlinedt.AirlineId = airlineId;
                var msg = new { Table = "Airline", Action = "Add", Data = airlinedt };
                quee.publish(msg);
                return Ok("Airline created successfully");

            }
            return BadRequest("Unable to create airline.");
        }
        [HttpGet("airline/getairline"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> AirlineDetails(int airlineId)
        {
            var airlineList = airlinedb.getAirline(airlineId);
            if (airlineList != null)
            {
                return Ok(airlineList);
            }
            else
            {
                return Ok("Airline not found");
            }
        }

        [HttpGet("airline/getallairline"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> AllAirlineDetails()
        {
            var airlineList = airlinedb.getAllAirline();
            if (airlineList != null)
            {
                return Ok(airlineList);
            }
            else
            {
                return Ok("Airline not found");
            }
        }

        [HttpPut("airline/status"), Authorize(Roles = "Admin")]
        
        public async Task<IActionResult> AirlineStatus(int airlineId, int active)
        {
            var result = airlinedb.updateStatus(airlineId,active);
            if(result)
            {


                var dataObj = new { AirlineId = airlineId, Active = active };
                var msg = new { Table = "Airline", Action = "Active",Data= dataObj };
                quee.publish(msg);

                return Ok("Airline status updated successfully");
            }
            else
            {
                return Ok("Failed to update status");
            }
        }
        [HttpDelete("airline/delete"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteAirline(int airlineId)
        {
            var result = airlinedb.dltairline(airlineId);
            if (result)
            {
                var newObj = new { AirlineId = airlineId };

                var msg = new { Table = "Airline", Action = "Delete", Data = newObj };
                quee.publish(msg);
                return Ok("Airline deleted successfully");
            }
            else
            {
                return Ok("Failed to delete airline");
            }

        }




    }
}
