﻿using AdminServices.Models;
using AdminServices.Repository;
using AdminServices.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Controllers
{
    [Route("admin/")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private IScheduleRepository scheduledb;
        private readonly IDatabaseSync quee;

        public ScheduleController(IConfiguration configuration, AdminDbContext adminDbCtx, IDatabaseSync dbquee)
        {
            _configuration = configuration;
            scheduledb = new ScheduleRepository(adminDbCtx);
            quee = dbquee;


        }
        [HttpPost("schedule/add"), Authorize(Roles = "Admin")]
        public async Task<ActionResult> AddSchedule(ScheduleData request)
        {
            var scheduledt = new Schedule();
            //MealsTypes key = (MealsTypes)Enum.Parse(typeof(MealsTypes), request.MealsType);
            scheduledt.AirlineId = request.AirlineId;
            scheduledt.FlightNumber = request.FlightNumber;
            scheduledt.FromPlace = request.FromPlace;
            scheduledt.ToPlace = request.ToPlace;
            scheduledt.StartDate = request.StartDate;
            scheduledt.EndDate = request.EndDate;
            scheduledt.ScheduledDays = request.ScheduledDays;
            scheduledt.BussinesSeats = request.BussinesSeats;
            scheduledt.TotalSeats = request.TotalSeats;
            scheduledt.TotalRows = request.TotalRows;
            scheduledt.InstrumentsUsed = request.InstrumentsUsed;
            scheduledt.MealsType = request.MealsType;
            scheduledt.TicketCost = request.TicketCost;
            scheduledt.IsActive = request.IsActive;

            int ScheId = scheduledb.AddSchedule(scheduledt);
            if (ScheId !=0)
            {
                scheduledt.ScheduleId = ScheId;
                var msg = new { Table = "Schedule", Action = "Add", Data = scheduledt };
                quee.publish(msg);
                return Ok("Schedule created successfully");

            }
            return BadRequest("Unable to create schedule.");

        }
        [HttpGet("schedule/getschedule"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> ScheduleDetails(int scheduleId)
        {
            var scheduleList = scheduledb.getSchedule(scheduleId);
            if (scheduleList != null)
            {
                return Ok(scheduleList);
            }
            else
            {
                return Ok("Schedule not found");
            }
        }

        [HttpGet("schedule/getallschedule"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> AllScheduleDetails()
        {
            var scheduleList = scheduledb.getallSchedule();
            if (scheduleList != null)
            {
                return Ok(scheduleList);
            }
            else
            {
                return Ok("Schedule not found");
            }
        }
        [HttpDelete("schedule/delete"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteSchedule(int scheduleId)
        {
            var result = scheduledb.dltschedule(scheduleId);
            if (result)
            {

                var newScheObj = new { ScheduleId = scheduleId };


                var msg = new { Table = "Schedule", Action = "Delete", Data = newScheObj };
                quee.publish(msg);
                return Ok("Schedule deleted successfully");
            }
            else
            {
                return Ok("Failed to delete schedule");
            }

        }
    }
}
