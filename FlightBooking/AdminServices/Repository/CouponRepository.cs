﻿using AdminServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Repository
{
    public class CouponRepository:ICouponRepository
    {
        private readonly AdminDbContext db;

        public CouponRepository(AdminDbContext dbx)
        {
            db = dbx;
        }
        public int AddCoupon(Coupons coupon)
        {
            try
            {
                var coupons = db.Coupons.Update(coupon);
                db.SaveChanges();
                return coupons.Entity.CouponId;
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        public Coupons getCoupon(int couponId)
        {
            return db.Coupons.Where(x => x.CouponId == couponId).FirstOrDefault();
        }

        public List<Coupons> getallCoupon()
        {
            var coupons = db.Coupons.ToList<Coupons>();
            return coupons;
        }
        public bool dltcoupon(int couponid)
        {
            try
            {
                Coupons couponobj = db.Coupons.First(x => x.CouponId == couponid);
                db.Coupons.Remove(couponobj);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
