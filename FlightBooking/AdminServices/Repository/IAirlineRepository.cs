﻿using AdminServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Repository
{
    public interface IAirlineRepository
    {
        int AddAirline(Airline airline);
        Airline getAirline(int airlineId);
        bool updateStatus(int airlineId, int active);
        bool dltairline(int airlineId);
        List<Airline> getAllAirline();
    }
}
