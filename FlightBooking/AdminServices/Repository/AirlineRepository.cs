﻿using AdminServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Repository
{
    public class AirlineRepository: IAirlineRepository
    {
        private readonly AdminDbContext db;

        public AirlineRepository(AdminDbContext dbx)
        {
            db = dbx;
        }

        public int AddAirline(Airline airline)
        {
            try
            {
                var al=   db.Airline.Update(airline);
                db.SaveChanges();
                return al.Entity.AirlineId;
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        public Airline getAirline(int airlineId)
        {
            return db.Airline.Where(x => x.AirlineId == airlineId).FirstOrDefault();
        }

        public List<Airline> getAllAirline()
        {
            var airlines= db.Airline.ToList<Airline>();
            return airlines;
        }

        public bool updateStatus(int airlineId, int active)
        {
            try
            {
                Airline airobj = db.Airline.First(x => x.AirlineId == airlineId);
                airobj.Active = active;
                db.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return false;
            }


        }
        public bool dltairline(int airlineId)
        {
            try
            {
                Airline airobj = db.Airline.First(x => x.AirlineId == airlineId);
                db.Airline.Remove(airobj);
                db.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}
