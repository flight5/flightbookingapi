﻿using AdminServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Repository
{
    public class ScheduleRepository:IScheduleRepository
    {
        private readonly AdminDbContext db;

        public ScheduleRepository(AdminDbContext dbx)
        {
            db = dbx;
        }
        public int AddSchedule(Schedule schedule)
        {
            try
            {
               var sched=  db.Schedule.Update(schedule);
                db.SaveChanges();
                return sched.Entity.ScheduleId;
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        public Schedule getSchedule(int scheduleId)
        {
            return db.Schedule.Where(x => x.ScheduleId == scheduleId).FirstOrDefault();
        }

        public List<Schedule> getallSchedule()
        {
            var schedule = db.Schedule.ToList<Schedule>();
            return schedule;
        }
        public bool dltschedule(int scheduleId)
        {
            try
            {
                Schedule scheduleobj = db.Schedule.First(x => x.ScheduleId == scheduleId);
                db.Schedule.Remove(scheduleobj);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
