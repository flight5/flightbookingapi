﻿using AdminServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Repository
{
    public interface ICouponRepository
    {
        int AddCoupon(Coupons coupon);
        Coupons getCoupon(int couponId);
        bool dltcoupon(int couponid);

        List<Coupons> getallCoupon();
    }
}
