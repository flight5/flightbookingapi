﻿using AdminServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Repository
{
    public interface IScheduleRepository
    {
        int AddSchedule(Schedule schedule);
        Schedule getSchedule(int scheduleId);
        List<Schedule> getallSchedule();
        bool dltschedule(int scheduleId);
    }
}
