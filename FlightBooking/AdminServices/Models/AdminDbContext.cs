﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Models
{
    public class AdminDbContext : DbContext
    {
        public AdminDbContext(DbContextOptions<AdminDbContext> options) : base(options)
        {
        }
        public DbSet<Airline> Airline { get; set; }
        public DbSet<Coupons> Coupons { get; set; }
        public DbSet<Schedule> Schedule { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Schedule>()
            .Property(u => u.MealsType)
            .HasConversion<string>()
            .HasMaxLength(50);
            modelBuilder.Entity<Coupons>()
                .HasIndex(p => new { p.CouponCode }).IsUnique();
        }
    }
}
