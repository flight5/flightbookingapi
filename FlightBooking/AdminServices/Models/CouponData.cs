﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Models
{
    public class CouponData
    {
        public string CouponCode { get; set; }
        public int Percentage { get; set; }
        public int MaxCount { get; set; }       
        public DateTime Expiry { get; set; }
    }
}
