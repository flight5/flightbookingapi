﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace AdminServices.Models
{
    public class Coupons
    {
        [Key]
        public int CouponId { get; set; }
        public string CouponCode { get; set; }
        public int Percentage { get; set; }
        public int MaxCount { get; set; }
        public int UsedCount { get; set; }
        public DateTime Expiry { get; set; }
    }
}
