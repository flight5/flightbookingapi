﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Models
{
    public class ScheduleData
    {
        public int AirlineId { get; set; }
        public string FlightNumber { get; set; }
        public string FromPlace { get; set; }
        public string ToPlace { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ScheduledDays { get; set; } = "Daily"; //Weakly,Daily,Monday,Tuesday,Wensday..etc
        public int BussinesSeats { get; set; }
        public int TotalSeats { get; set; }
        public int TotalRows { get; set; }
        public string InstrumentsUsed { get; set; }
        public MealsTypes MealsType { get; set; }
        public Double TicketCost { get; set; }

        public int IsActive { get; set; } = 1;
    }
    
}
