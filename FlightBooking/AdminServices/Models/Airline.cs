﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Models
{
    public class Airline
    {
       
        [Key]
        public int AirlineId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Adress { get; set; }
        public string logo { get; set; }
        public int Active { get; set; } = 1;
        public DateTime createdDate { get; set; } = DateTime.Now;

    }
}
