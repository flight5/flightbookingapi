﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminServices.Models
{
    public class AirlineData
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Adress { get; set; }
        public string logo { get; set; }
        public int Active { get; set; } = 1;
    }
}
