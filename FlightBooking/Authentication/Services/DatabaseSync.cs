﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Services
{
    public class DatabaseSync : IDatabaseSync
    {
        private IConnection connection;
        private string connectionString;
        public string queeName { get; set; }

        public DatabaseSync()
        {
            connectionString = "amqp://guest:guest@localhost:5672";
            queeName = String.IsNullOrEmpty(queeName) ? "FLBDBSYNC" : queeName;
            CreateConnection();
        }

       public void publish(Object message )
        {
            if (ConnectionExists())
            {
                using (var channel = connection.CreateModel())
                {
                        channel.QueueDeclare(queeName,
                            durable: false,
                            exclusive: false,
                            autoDelete: false,
                            arguments: null
                        );
                        var json = JsonConvert.SerializeObject(message);
                        var body = Encoding.UTF8.GetBytes(json);

                       channel.BasicPublish(exchange: "", routingKey: queeName, basicProperties: null, body: body);
                }
            }
        }


        private void CreateConnection()
        {
            try
            {
                var factory = new ConnectionFactory
                {
                    Uri = new System.Uri(connectionString)
                };

                 connection = factory.CreateConnection();
            }
            catch(Exception e)
            {
                Console.WriteLine("cannot create connection:{e.message}");
            }
        }
        private bool ConnectionExists()
        {
            if (connection != null)
            {
                return true;
            }

            CreateConnection();

            return connection != null;
        }

    }
}
