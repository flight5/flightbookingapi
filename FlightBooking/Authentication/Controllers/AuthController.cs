﻿using Authentication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Authentication.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Authentication.Services;

namespace Authentication.Controllers
{
    [Route("authorize/")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private IUserRepository userdb;
        private readonly IDatabaseSync quee;

        public AuthController(IConfiguration configuration,UserDbContext userDbCtx, IDatabaseSync QueOBj)
        {
            _configuration = configuration;
            userdb = new UserRepository(userDbCtx);
            quee = QueOBj;

        }
        [HttpPost("register")]
        public async Task<ActionResult> Register(UserData request)
        {

          
            var userdt = new User();

            CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);

            userdt.Name = request.Name;
            userdt.Email = request.Email;
            userdt.PhoneNumber = request.PhoneNumber;
            userdt.Gender = request.Gender;
            userdt.Username = request.Username;
            userdt.Password = Convert.ToBase64String(passwordHash);
            userdt.PasswordSalt = Convert.ToBase64String(passwordSalt);
            userdt.Role = request.Role;


            int userId = userdb.Adduser(userdt);
            if (userId !=0) {
                userdt.UserId = userId;

                var msg = new { Table = "Customer", Action = "Add", Data = userdt };
                quee.publish(msg);
                return Ok("User created scucessfully");

            }

            return BadRequest("Unbale to create user.");

        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login(LoginData ldata)
        {
            User user = new User();
            //var userdt = new UserDto();
            var userName = ldata.userName;
            var password = ldata.password;
            if (userName == "" || userName == null) {
                return BadRequest("Please provide valid user name");

            }
            if (password == "" || password == null)
            {
                return BadRequest("Please provide valid user name");

            }

            User udata = userdb.getUserDetails(userName);

            if (udata ==null)
            {
                return BadRequest("User not found");

            }

            if(!VerifyPasswordHash(password,Convert.FromBase64String(udata.Password), Convert.FromBase64String(udata.PasswordSalt)))
            {
               
                return BadRequest("Wrong Password");
            }
            
            string token     = CreateToken(udata);
            var refreshToken = GenerateRefreshToken(udata.UserId);
            var tokenDetails = new RefreshToken();
            var tkn = new { access_token = token, refresh_token = refreshToken.Token,expiry=refreshToken.Expires };
            return Ok(tkn);

        }
        private string CreateToken(User user)
        {
           
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Role),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.UserData, ""+user.UserId),
                new Claim(ClaimTypes.MobilePhone, user.PhoneNumber)


            };
            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(
                _configuration.GetSection("Appsettings:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds);
            var jwt = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }
        }
        private RefreshToken GenerateRefreshToken(int userId)
        {
            var randomnum = new byte[64];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomnum);
            var refreshToken = new RefreshToken
            {
                Token = Convert.ToBase64String(randomnum),
                Created = DateTime.Now,
                Expires = DateTime.Now.AddDays(2),
                userId = userId

            };
            userdb.saveRefereshToken(refreshToken);

            return refreshToken;
        }

        private void SetRefreshToken(RefreshToken newRefreshToken)
        {
            var userdt = new UserDto();
            var cookiesOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = newRefreshToken.Expires
            };
            Response.Cookies.Append("refreshToken", newRefreshToken.Token, cookiesOptions);
            userdt.RefreshToken = newRefreshToken.Token;
            userdt.TokenCreated = newRefreshToken.Created;
            userdt.TokenExpires = newRefreshToken.Expires;
        }


        [HttpPost("token/refresh")]
        public async Task<ActionResult<string>> RefreshToken(String refershToken)
{

    

            if (refershToken.IsNullOrEmpty()) {
                return BadRequest("Not a valid token");

            }

            RefreshToken tokenDetails = userdb.getRefreshToken(refershToken);

            if (tokenDetails ==null)
            {
                return Unauthorized("Invalid Refresh Token");
            }
            else if (tokenDetails.Expires < DateTime.Now)
            {
                return Unauthorized("Token Expired");
            }

            User udata = userdb.getUserById(tokenDetails.userId);

            if (udata == null)
            {
                return Unauthorized("Unbale to found user");

            }
            userdb.removeRefreshToken(tokenDetails);

            string token = CreateToken(udata);
            RefreshToken newRefreshToken = GenerateRefreshToken(tokenDetails.userId);
            var tkn = new { access_token = token, refresh_token = newRefreshToken.Token, expiry = newRefreshToken.Expires };
            return Ok(tkn);

        }


    }
}
