﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authentication.Models;

namespace Authentication.Repository
{
    public interface IUserRepository
    {
        int Adduser(User user);
        User getUserDetails(String userName);
        void saveRefereshToken(RefreshToken token);
        RefreshToken getRefreshToken(String token);
        User getUserById(int userId);
        void removeRefreshToken(RefreshToken token);
    }
}
