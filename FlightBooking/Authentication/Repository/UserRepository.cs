﻿using Authentication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Authentication.Repository
{
    public class UserRepository: IUserRepository
    {
        private UserDbContext db;
        public UserRepository(UserDbContext context)
        {
            db = context;
        }
        public int Adduser(User user)
        {
            try
            {
                var userDb = db.User.Update(user);
                db.SaveChanges();
                return userDb.Entity.UserId;
            }
            catch (Exception e) {
                return 0;
            }
            

        }

        public User getUserDetails(String userName)
        {
            try
            {
                User usedata = db.User.Where(x=>x.Username.Equals(userName)).FirstOrDefault();

                return usedata;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void saveRefereshToken(RefreshToken token)
        {
            try
            {
                db.RefreshToken.Update(token);
                db.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
        public RefreshToken getRefreshToken(String token)
        {
            try
            {
                RefreshToken tokenData = db.RefreshToken.Where(x => x.Token.Equals(token)).FirstOrDefault();

                return tokenData;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public User getUserById(int userId)
        {
            try
            {
                User usedata = db.User.Where(x => x.UserId == userId).FirstOrDefault();

                return usedata;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public void removeRefreshToken(RefreshToken token)
        {
            try
            {
                db.RefreshToken.Remove(token);
                db.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }




    }
}
